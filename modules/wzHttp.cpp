/*
  Name: wzHttp.cpp
  Date: 23.11.2006
  Description: Functions for http-request processing.
*/


#include "wzHttp.h"


/** ����� HTTP-��������� */

long GetHttpHeaderLen(string sHttpRequest)
{
	dbgmsg("long getHeaderLen(char* request)");

	return GetHttpHeader(sHttpRequest).length();
}


/** ��������� HTTP-��������� �� ������� */

string GetHttpHeader(string sHttpRequest)
{
	dbgmsg("void getHeader(char* header, char* request, long iSize)");

	if (sHttpRequest.length() < 10) return string("");

	string sBr;
	sBr += char (0x0D);
	sBr += char (0x0A);
	sBr += char (0x0D);
	sBr += char (0x0A);
	
	// ���� ����� �������
	int iPos = sHttpRequest.find(sBr);
	if (iPos <= 0) return string("");

	// ��������� ������. ������ ��� ��� ���
	if (sHttpRequest.substr(0, 3) == "GET"
		|| sHttpRequest.substr(0, 4) == "POST"
		|| sHttpRequest.substr(0, 8) == "HTTP/1.0"
		|| sHttpRequest.substr(0, 8) == "HTTP/1.1") return sHttpRequest.substr(0, iPos + 4);
	
	return string("");
}


/** ������������� HTTP-���������, ���� ������ ��������� � �������� ���������-������
  � GET ������ ��������� ����� �����, ������ Proxy-Connection �� Connection. */

string CorrectingHeader (string sHttpRequest)
{
	dbgmsg("bool CorrectingHeader(string sHttpRequest)");

	string sHttpHeader = GetHttpHeader (sHttpRequest);

	dbgmsg("header before modification: " + sHttpHeader);

	string sBr;
	sBr += char (0x0D);
	sBr += char (0x0A);

	vector<string> vsHttpHeaderParams = StrExplode(sHttpHeader, sBr);
	string sHttpBody = sHttpRequest.substr (sHttpHeader.size(), sHttpRequest.size() - sHttpHeader.size());
	
	int iStart, iEnd;

	string sHost, sUrl;
	vector<string> vsParams = StrExplode(vsHttpHeaderParams.at(0), " ");
	// ������ � ����� MyIE (/http://ya.ru/zxc/  HOst: localhost:4321).
	// ������������ ������ ���� Host (��������� ��� �����, � �� Web2zipClient).
	// � �������� ������ (���� ����): "/http" -> "http"
	if (vsParams.at(1).size() > 10)
		if (vsParams.at(1).substr(0, 6) == "/http:")
		{
			dbgmsg("/http: found");			// /http://ya.ru/zxc/
			sHost = vsParams.at(1).substr(8);	// ya.ru/zxc/
			dbgmsg("sHost N1=" + sHost);		//
			int i = sHost.find("/");		//
			dbgmsg("i=" + IntToStr(i));		// 
			dbgmsg("sHost.substr(i)=" + sHost.substr(i));
			dbgmsg("vsParams.at(1)=" + vsParams.at(1));
			sHost = sHost.substr(0, i > 0 ? i : 0);	// ya.ru
			dbgmsg("sHost N2=" + sHost);

			vsHttpHeaderParams.at(0) = vsParams.at(0) + " " + vsParams.at(1).substr(1) + " " + vsParams.at(2);
			dbgmsg("vsHttpHeaderParams.at(0)=" + vsHttpHeaderParams.at(0));
		}
		else dbgmsg("/http: NOT found");

	dbgmsg("sHost=" + sHost);

	// ������: �������� http://zxc.zx �� URL http://zxc.zx/asd 
	iStart = vsHttpHeaderParams.at(0).find("http://");
	iEnd = vsHttpHeaderParams.at(0).find("/");
	iEnd = vsHttpHeaderParams.at(0).find("/", iEnd + 2);
	if ((iStart >= 0) && (iEnd >= 0)) vsHttpHeaderParams.at(0).erase(iStart, iEnd - iStart);

	string sRet;
	// �������� �� ���� ������� HTTP-���������
	for(vector<string>::iterator ivsIter = vsHttpHeaderParams.begin(); ivsIter != vsHttpHeaderParams.end(); ++ivsIter)
	{
		vector<string> vsParam = StrExplode(*ivsIter, string(":"));

		// �������� proxy-connection �� Connection
		if (vsParam.size() >= 1)
			if (StrLCase(vsParam.at(0)) == "proxy-connection")
				if(vsParam.size() == 2)
					*ivsIter = string("Connection: Close");// + vsParam.at(1);

		// �������� �������� Host �� ��, ��� ����� �� URL, ���� ������ � ����� MyIE
		if (sHost.length() > 0)
			if (StrLCase(vsParam.at(0)) == "host")
				if(vsParam.size() > 1)
					*ivsIter = string("Host: ") + sHost;

		sRet += *ivsIter + sBr;
	}

	dbgmsg("header after modification: " + sRet);

	sRet += sBr + sHttpBody;

	return sRet;

/* bphoenix edition
	dbgmsg("bool CorrectingHeader(string sHttpRequest)");
	
	dprintf( "watch before correction: %s\n", (char *) sHttpRequest.c_str() );


	string sBr;
	sBr += char (0x0D);
	sBr += char (0x0A);

   	string sHttpHeader = GetHttpHeader (sHttpRequest);
    string sHttpBody = sHttpRequest.substr (sHttpHeader.size(), sHttpRequest.size() - sHttpHeader.size());


	vector<string> vsHttpHeaderParams = StrExplode(sHttpRequest, sBr);
	int iStart, iEnd;
	string sHeader;
	iStart = StrLCase( vsHttpHeaderParams.at( 0 ) ).find( "/http://" );
	if ( iStart != std::string::npos )
	   iEnd = 8;
    else {
	   iStart = StrLCase( vsHttpHeaderParams.at( 0 ) ).find( "http://" );
       if ( iStart != std::string::npos )
          iEnd = 7;
       else
           return sHttpRequest;
    }    
        
    vsHttpHeaderParams.at( 0 ).erase( iStart, iEnd );
    iEnd = vsHttpHeaderParams.at( 0 ).find( "/" ) + 1;
    int i = vsHttpHeaderParams.at( 0 ).rfind( " " );
    if ( iEnd > i || iEnd == std::string::npos )
       iEnd = i;
       
    string sHost = vsHttpHeaderParams.at( 0 ).substr( iStart, iEnd - iStart );
    vsHttpHeaderParams.at( 0 ).replace( iStart, iEnd - iStart, "/" );

    iStart = sHost.rfind( "/" );
    if ( iStart != std::string::npos )   
        sHost.erase( iStart );
        
    sHeader +=  vsHttpHeaderParams.at( 0 ) + sBr;
    sHeader += "Host: " + sHost + sBr;
    
    bool bKAfound = false;
    for( i = 1; i < vsHttpHeaderParams.size(); i++ ) {
         string sParam = vsHttpHeaderParams.at( i );
         string sLCase = StrLCase( sParam );
         if ( sLCase.find( "proxy-connection:" ) == 0 )
            sParam.replace( 0, 16, "Connection" );
         else if ( sLCase.find( "host:" ) == 0 )
              continue;
         else if ( sLCase.find( "Keep-Alive:" ) == 0 )
              bKAfound = true;
              //sParam = "Host: " + sHost;
         sHeader += sParam + sBr;
    } 
    
    if (!bKAfound)
       sHeader += "Keep-Alive: 300" + sBr;
       
       
    sHeader += sBr + sHttpBody; 
    
	dprintf( "watch after correction: %s\n", (char *) sHeader.c_str() );    	    
    return sHeader;
*/
}


/** ��������� ��������� �� HTTP-���������. ��� ��������� ����� sParamName="Port". 
 * �������� Host ������������ ��� ������ ����� (�.�. ������� ya.ru:8080 ����������).
 * @param sParamName - ��� ����
 * @param sHttpRequest - Http-��������� */

string GetParameterFromHttpHeader(string sParamName, string sHttpRequest)
{
	dbgmsg("bool getParameterFromHttpHeader(char* name, char* value, char* request)");

	string sHttpHeader = GetHttpHeader(sHttpRequest);
	if (sHttpHeader.length() <= 0) return string("");

	string sBr;
	sBr += char (0x0D);
	sBr += char (0x0A);
	
	vector<string> vsHttpHeaderLines = StrExplode(sHttpHeader, sBr);
	dbgmsg("vsHttpHeaderLines.size="); dbgmsg(vsHttpHeaderLines.size());

	if (vsHttpHeaderLines.size() < 2) return string("");

	// �� ���� ������� � Http-���������, �� ����������� ������ "GET http://zxc.ru HTTP/1.0"
	for(int i = 1; i < vsHttpHeaderLines.size(); i++)
	{
		vector<string> vsParam = StrExplode(vsHttpHeaderLines.at(i), string(":"));
		if(vsParam.size() < 2) continue;

		vsParam.at(0) = StrTrim(vsParam.at(0));
		vsParam.at(1) = StrTrim(vsParam.at(1));

		// ������ ������ �����
		if (sParamName == "Port" && StrLCase(vsParam.at(0)) == "host")
		{
			// ������ ����?
			if (vsParam.size() == 3) return vsParam.at(2);
			else return string("80");
		}

		// ������ ����� �����
		if (sParamName == "Host" && StrLCase(vsParam.at(0)) == "host")
			return vsParam.at(1);

		// ���������� ������ � ����������� �������� (��� ����� ���������� ":")
		for(int j = 2; j < vsParam.size(); j++)
			vsParam.at(1) += string(":") +StrTrim(vsParam.at(j));

		// ����� ��������
		if (StrLCase(vsParam.at(0)) == StrLCase(string(sParamName)) && vsParam.size() > 1)
		{
			return vsParam.at(1);
		}
	}

	return string("");
}


/** ��������� ����� ����� �� HTTP-������� */

string GetHostFromHttpRequest(string sHttpRequest)
{
	dbgmsg("string GetHostFromHttpRequest(string sHttpRequest)");

	return GetParameterFromHttpHeader(string("Host"), sHttpRequest);
}


/** ��������� ����� �� HTTP-������� */

unsigned int GetPortFromHttpRequest(string sHttpRequest)
{
	dbgmsg("long getPortFromRequest(char* request)");

	return StrToDbl(GetParameterFromHttpHeader(string("Port"), sHttpRequest));
}


/** ������ ����� http-�������
 ���� ������������ -1, �� ������ �� ��������� ��� http */

int GetRequestSize(string sHttpRequest)
{
	dbgmsg("int getRequestSize(string sHttpRequest)");

	if (sHttpRequest.length()  < 5)
	{
		dbgmsg("sHttpRequest.length() < 5! exit");
		return -1;
	}

	// ��������� ���?
	if (GetHttpHeader(sHttpRequest).length() <= 10)
	{
		dbgmsg("GetHttpHeader(string(request)).length() <= 10! exit");
		return -1;
	}
	else
	{
		string sValue = GetParameterFromHttpHeader("Content-Length", sHttpRequest);
		// Content-Length �����������? - ��� ������ GET
		if (sValue.size() <= 0)
		{
			dbgmsg("Content-Length - No. Its GET req");
			return GetHttpHeaderLen(sHttpRequest);
		}
		// ��� ������ POST
		else
		{
			// ��� ������, ��������� 2??? ����������� �������� 0d 0a
			if (sHttpRequest.substr(0, 4) == "POST")
			{
				dbgmsg("its POST req");
				// ������������ ������������� ������� POST �������. �� ���-�� ����������� �������� � ������ �������� ������
				int iLen = GetHttpHeaderLen(sHttpRequest) + StrToDbl(sValue);
				if (iLen < 0) return -1;

				if (iLen < sHttpRequest.size()) iLen = sHttpRequest.size();
				return iLen;
			}
			// ��� �����
			else if (sHttpRequest.substr(0, 4) == "HTTP")
			{
				dbgmsg("HTTP answ");
				return GetHttpHeaderLen(sHttpRequest) + StrToDbl(sValue);
			}
			// ���������� ������
			else
			{
				dbgmsg("unknow request");
				return -1;
			}
		}
	}
	return -1;
}


/** ��������� http ���� ������ �� ���������� ������ */

int getHttpAnswerCode(string sHttpRequest)
{
	dbgmsg("int getHttpAnswerCode(string sHttpRequest)");
	char szOp[4];
	szOp[0] = sHttpRequest.at(9);
	szOp[1] = sHttpRequest.at(10);
	szOp[2] = sHttpRequest.at(11);
	szOp[3] = '\0';

	if ('0' <= szOp[0] && szOp[0] <= '9'
	        && '0' <= szOp[1] && szOp[1] <= '9'
	        && '0' <= szOp[2] && szOp[2] <= '9') return atoi(szOp);

	return 0;
}


/** ��������������� IP � ������,  */

string IpIntToStr(int iIp)
{
	int a, b, c, d;
	a=(iIp & 0x000000ff) >> 0x00;
	b=(iIp & 0x0000ff00) >> 0x08;
	c=(iIp & 0x00ff0000) >> 0x10;
	d=(iIp & 0xff000000) >> 0x18;

	stringstream ssBuff;
	string sRet;
	ssBuff << a << "." << b << "." << c << "." << d;
	ssBuff >> sRet;

	return sRet;
}


/** ��������� URL �� ������� */

string GetUrlFromHttpRequest(string sHttpRequest)
{
	dbgmsg("string GetUrlFromHttpRequest(string sHttpRequest)");

	string sBr;
	sBr += char(0x0D);
	sBr += char(0x0A);
	int iPos = sHttpRequest.find(sBr);
	if (iPos <= 0) return string("");
	
	// �������� ������ ������ Http-���������
	vector<string> vsHttpParam = StrExplode(sHttpRequest.substr(0, iPos), string(" "));

	// ���� ������� ��� ���������
	if (vsHttpParam.size() == 3) return vsHttpParam.at(1);
	else return string("");
}


/** �������� �� URL ���������� �������������� �����. */

string GetFileExtensionFromHttpRequest(string sHttpRequest)
{
	dbgmsg("string getFileExtensionFromRequest(string sHttpRequest)");

	string sUrl = GetUrlFromHttpRequest(sHttpRequest);
	if (sUrl.length() <= 0) return false;

	// �������� ��������� ���� ����
	int iQPos = sUrl.find(string("?"));
	if (iQPos >= 0) sUrl = sUrl.substr(0, iQPos);

	// ���� ����� � �������� ��� ��� �� ���
	int iDPos = sUrl.rfind(string("."));
	if (iDPos >= 0) sUrl = sUrl.substr(iDPos + 1, sUrl.length() - iDPos);
	
	// ���� ������ �������� ����� �� ���������� ������ ������
	if ( (int(sUrl.find(string("/"))) >= 0) || (int(sUrl.find(string("\\"))) >= 0) ) return string("");
	
	return sUrl;
}
