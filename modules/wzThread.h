/*
  Name: wzThread.h
  Date: 18.05.2007
  Description: Thread abstract class. Provides cross-platform unified interface
   for thread manipulation.
*/


#ifndef _WZ_WZTHREAD_H_
#define _WZ_WZTHREAD_H_

#include <iostream>

#ifdef WIN32
	#include <windows.h>
	#include <pthread.h>
#else
	#include <pthread.h>
	typedef pthread_t HANDLE;
#endif

using namespace std;

/** ����������� ����� �����. ������������ ��������������� ���������
     ������ � ��������. ���������� ������� POSIX pthread � WINAPI thread.
     ������� ����� ������� ��������� ������� ThreadFunction. */

class wzThread
{
	public:
		/** �����������. */
		wzThread();
		
		/** ����������. ������������� ��������� ��������� �����. */
		~wzThread();
		
	private:
		/** ����������� ������� ������� ���������� ���������� ����� �������� ������. ������ �-�� ��������� ��� ThreadFunction. */
		static void* ThreadStart(void*);
		
		/** ������� ������� ��������� ��������� � ��������� ������. */
		virtual void ThreadFunction(void) = 0;
		
	protected:
		/** ������������� ���������� ������. */
		HANDLE m_hThreadId;
};

#endif
