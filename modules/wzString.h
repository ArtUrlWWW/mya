/*
  Name: wzString.h
  Date: 17.05.2006
  Description: wzString class + some useful functions
*/


#ifndef _WZ_STRING_H_
#define _WZ_STRING_H_

#include <math.h>
#include <sstream>
#include <string.h>
#include <string>
#include <vector>
#include "debug.h"

class wzString : public string
{
	public:
		// ������������ ������ � double
		operator double ();

		// ������������ double-�������� � ������
		wzString operator = (double dValue);
};

/** �������������� ������ � ����� */

double StrToDbl(string sValue);


/** �������������� ����� � ������ */

string DblToStr(double dValue, int iDecimal = -1);

/** Converts an integer value to a string */

string IntToStr(int iValue);

/** Converts an integer value to a string */

string LongLongToStr(long long llValue);
/** Converts an string to a integer value */

long long StrToLongLong(string sValue);
/** Converts an string to a integer value */

int StrToInteger(string sValue);

/** ������� ������ �� ������ �����
 *   @param bSkipVoid = true - �� ��������� ������ ������ � ������ 
 *   @param sMain - ������ ������� ��������� �������
 *   @param sBr - ������ �����������
 */

vector<string> StrExplode(string sMain, string sBr, bool bSkipVoid = true);


/** ������� ������ ����� � ������ 
 *   @param vsMain - ������ �����
 *   @param sBr - �����������
 *   @param bSkipVoid - �� ��������� ������ ������-�������� �� �������
 */

string StrJoin(vector<string> vsMain, string sBr, bool bSkipVoid = true);


/** �������� ������� �� ����� ������ */

string StrTrim(string sMain);


/** ��������� ��������� ������� � ������ �������. 
 * ����� ������� ������������� �������� ���������, ��������� ����� ������� ���������. */

string StrLCase(string sMain);


/** �������� ������������ �� ������� � ������� */

bool IsInSet(string sItem, vector<string> vsVec);


/** ������ ��������� �� ��������.
 * ������: StrReplace("1234", "3", "x") = "12x4" */

string StrReplace(string sMain, string sFind, string sReplace);

#endif
