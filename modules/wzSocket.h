/*
  Name: wzSocket.h
  Date: 17.05.2007
  Description: Crossplatform socket class (POSIX, Windows)
*/


#ifndef _WZ_WZSOCKET_H_
#define _WZ_WZSOCKET_H_

#include <string>
#include <iostream>
#include <sys/timeb.h>

#ifdef WIN32
	#ifndef MSG_NOSIGNAL
		#define MSG_NOSIGNAL 0
	#endif

	#include <winsock2.h>
	#ifndef socklen_t
		typedef int socklen_t;
	#endif
#else
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <fcntl.h>
	#ifndef SOCKET
		typedef int SOCKET;
	#endif
#endif

using namespace std;


/*enum WZ_SOCKET_ENUM_STATE
{
	WS_NOT_CONNECT = 0,
	WS_CONNECT = 1,
	WS_LISTEN = 2,
	WS_CLOSE = 3
};*/

/** ����� ������� ��� �������. ������������ ��������������� ��������� � �������. */
class wzSocket
{
	public:
		/** ����������� */
		wzSocket();
		
		/** ���������� */
		~wzSocket();
		
		/** �������� ������ */
		bool Send (string sData);
			
		/** ������� ������ */
		bool Recv (string& sData, int iMaxLen);

		/** �������� ����� ��  ������������� 
		@return true - �������� ����������� ������ */
		bool Listen(int iPort);

		/** ������� ����������� �� ����� wsAccept. ���������������, ��� ������� ����� 
		��������� � ������ ���������. */
		bool Accept(wzSocket& wsAccept);

		/** ����������� � ����� */
		bool Connect(string sHostname, int iPort);

		/** ������� ����� */
		bool Close();

		/** ������� TCP ����� ��� ������ */
		bool ShutdownRD();

		/** ������� TCP ����� ��� ������ */
		bool ShutdownWR();

		/** ������� TCP ����� ��� ������ � ������ */
		bool ShutdownRDWR();

		/** ���������� ������� �� �����. 0 - ����������� ������� */
		void SetRecvTimeout(unsigned int sec, unsigned int usec);

		/** ������������ ������� */
		void operator = (wzSocket sckNew);

		// Returns IP of the socket
		unsigned int ip();
		
		/** ���������� ������ */
		SOCKET hSocket;

	private:
		/** IP ������� ���� ����� ������ ��������� �������. ����� �������� 0. */
		unsigned int m_IP;
		
		/** ���� ���������. �������� �� ������ ����� ��������� 
		@return true - ��������� �����,
		false - ������� ����� */		
		bool m_bIsListen;
};

#endif
