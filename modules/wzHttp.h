/*
  Name: wzHttp.h
  Date: 23.11.2006
  Description: Functions for http-request processing.
*/


#ifndef _WZ_wzHTTP_H_
#define _WZ_wzHTTP_H_

#include <string.h>
#include <string>
#include <sstream>
#include "debug.h"
#include "wzString.h"

/** ������������ ����� ������ � http-���������, ���� ������ ������, �� �� �������������� */
#define WZ_MAX_HEADER_LINE_LEN 2000

using namespace std;


/** ����� HTTP-���������. */

long GetHttpHeaderLen(string sHttpRequest);


/** ��������� HTTP-��������� �� �������. */

string GetHttpHeader(string sHttpRequest);


/** ������������� HTTP-���������, ���� ������ ��������� � �������� ���������-������
  � GET ������ ��������� ����� �����, ������ Proxy-Connection �� Connection. */

string CorrectingHeader (string sHttpRequest);


/** ��������� ��������� �� HTTP-���������. ��� ��������� ����� sParamName="Port". 
 * �������� Host ������������ ��� ������ ����� (�.�. ������� ya.ru:8080 ����������).
 * @param sParamName - ��� ����
 * @param sHttpRequest - Http-��������� */

string GetParameterFromHttpHeader(string sParamName, string sHttpRequest);


/** ��������� ����� ����� �� HTTP-������� */

string GetHostFromHttpRequest(string sHttpRequest);


/** ��������� ����� �� HTTP-������� */

unsigned int GetPortFromHttpRequest(string sHttpRequest);


/** ������ ����� http-�������
 ���� ������������ -1, �� ������ �� ��������� ��� http */

int GetRequestSize(string sHttpRequest);


/** ��������� http ���� ������ �� ���������� ������. */

int getHttpAnswerCode(char* request);


/** ����������� ������� ��������� ���������. */

int instr(int pos, char* src, char* find);


/** ����������� IP int � ������. */

string IpIntToStr(int iip);


/** ��������� URL �� ������� */

string GetUrlFromHttpRequest(string sHttpRequest);


/** �������� �� URL ���������� �������������� �����. */

string GetFileExtensionFromHttpRequest(string sHttpRequest);

#endif
