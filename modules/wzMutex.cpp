/*
  Name: wzMutex.cpp
  Date: 19.05.2007
  Description: Crossplatform mutex class (POSIX, Wwindows)
*/


#include "wzMutex.h"

/** Constructor. */
wzMutex::wzMutex()
{
	dbgmsg("wzMutex::wzMutex()");
	#ifdef WIN32
		static char sName[] = "WZMUTEX";
		gcvt(GetTickCount(), 9, sName + strlen("WZMUTEX"));
		m_sMutexName = sName;
		m_mutex = CreateMutex(NULL, false, m_sMutexName.c_str());
		dbgmsg("m_mutex=" + IntToStr(int(m_mutex)));
	#else
		pthread_mutex_init(&m_mutex, NULL);
	#endif
}
		
/** Destrictor. */
wzMutex::~wzMutex()
{
	dbgmsg("wzMutex::~wzMutex()");
	Unlock();
	#ifdef WIN32
		CloseHandle(m_mutex);
	#else
		pthread_mutex_destroy(&m_mutex);
	#endif
}
		
/** Lock mutex. */
bool wzMutex::Lock()
{
	dbgmsg("bool wzMutex::Lock()");
	#ifdef WIN32
		WaitForSingleObject(m_mutex, INFINITE);
		dbgmsg("mutex handle=" + IntToStr(int(m_mutex)));
	#else
		pthread_mutex_lock(&m_mutex);
	#endif

	return true;
}

/** Unlock mutex. */
bool wzMutex::Unlock()
{
	dbgmsg("bool wzMutex::Unlock()");
	#ifdef WIN32
		bool b = ReleaseMutex(m_mutex);
		dbgmsg("release mutex ok=" + IntToStr(int(b)));
	#else
		pthread_mutex_unlock(&m_mutex);
	#endif

	return true;
}
