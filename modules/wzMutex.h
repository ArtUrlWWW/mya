/*
  Name: wzMutex.h
  Date: 19.05.2007
  Description: Crossplatform mutex class (POSIX, Wwindows)
*/


#ifndef _WZ_WZMUTEX_H_
#define _WZ_WZMUTEX_H_

#include <string>
#include "debug.h"
#include "wzString.h"

#ifdef WIN32
	#include <windows.h>
#else
	#include <pthread.h>
#endif

using namespace std;

/** Class Mutex */
class wzMutex
{
	public:
		/** Constructor. */
		wzMutex();
		
		/** Destrictor. */
		~wzMutex();
		
		/** Lock mutex. */
		bool Lock();

		/** Unlock mutex. */
		bool Unlock();

	private:
	
	#ifdef WIN32
		HANDLE m_mutex;
		string m_sMutexName;
	#else
		pthread_mutex_t m_mutex;
	#endif
			
};

#endif
