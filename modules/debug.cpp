/**
 17.11.2006
 Debug functions

 12.02.2007
 FIXME: logfile-cout - ����������� ��� �������� ����. ���� ��������� ��
 ��������� �� �� ��������, �� � ����, �� ��������� �������� ��� ��������� �����
 ����������. ������� ���� ���� �� ����.

 ��� ���� ����� �������� ��� ��������� �������������� gcc ������ ������
  � ���������� -DDEBUG.
 ��� ������ ����� ���-����� (��� �� �������, �������� ������������ 
  �� �����) � ���� ��� SIGAV ������� �������� -DDEBUG2.
  
 FIXME: ��������� DEBUG � DEBUG2 �����������������
*/

#ifdef DEBUG
 #ifdef DEBUG2
  #error parameters DEBUG and DEBUG2 mutual exclusive
 #endif
#endif



#include "debug.h"
#include <time.h>


/** ����� ������� �������. */
wzdebug wzdebug;

wzdebug::wzdebug()
{
#ifdef DEBUG
	pid_t pid;

	// ��� ����� ������� �� ������ �������� �������
	char sFileName[50] = "0";
	time_t now;
	now = time((time_t*) 0);
	strftime(sFileName, 50, WZ_SERV_RFC1123FMT_FILENAME, gmtime(&now));
	strcat(sFileName, "GMT.log");

	m_logfile.open(sFileName);
	m_logfile << "This file contains Web2zip log info.\n"
	<< "<thread id> date : message\n\n";
#endif
}


/**  ���������� ������� �������. */

wzdebug::~wzdebug()
{
#ifdef DEBUG
	m_logfile.close();
#endif
}


/** ���������� ����� �����. */

void DbgSaveDump(void)
{
#ifdef DEBUG2
	// ��� ����� ������� �� ������ �������� �������
	char sFileName[50] = "0";
	time_t now;
	now = time((time_t*) 0);
	strftime(sFileName, 50, WZ_SERV_RFC1123FMT_FILENAME, gmtime(&now));
	strcat(sFileName, "GMT-dump.log");

	wzdebug.m_logfile.open(sFileName);
	wzdebug.m_logfile << "This file contains Web2zip log-dump info.\n"
	<< "<thread id> date : message\n\n";

	// ��������� ���� ����
	for(int i = 0; i < wzdebug.m_logdeq.size(); i++)
		wzdebug.m_logfile << wzdebug.m_logdeq.at(i);
	
	wzdebug.m_logfile.close();
	sleep(1);
#endif
}


/** ��������� ������� � ������� RFC.
    @param sizeof(timebuff) >= 50 */

string gettime(void)
{
	char szTimeBuff[1000];
	time_t tNow;
	tNow = time((time_t*) 0);
	(void) strftime(szTimeBuff, 50, WZ_SERV_RFC1123FMT, gmtime(&tNow));
 
	return string(szTimeBuff);
}


/** ��������� ��������� */

void stop()
{
	stringstream ss;

	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : Program stoped. Exit\n";

#ifdef DEBUG
	wzdebug.m_logfile << ss.str();
#else
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif	
#endif

	exit(1);
}


/** ����� ��������� � ����� printf. */

void dprintf(string fmt, ...)
{
	va_list arg;
	
	va_start( arg, fmt );
	char str[ 1000 ];
	vsnprintf( &str[ 0 ], 1000, (char *) fmt.c_str(), arg );
	
	stringstream ss;

	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : " << str << endl;
	
#ifdef DEBUG
	// ���� ���������� ����� ������ 10 000 000, �� �� ������� ���.
	// ������ �� ������������ ��������� � ������������ ������ ����
	static int iAmountLines = 0;
	iAmountLines++;
	if(iAmountLines > 1E+7) return;

	wzdebug.m_logfile << ss.str();
	wzdebug.m_logfile.flush();
	
#else
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
 	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif
#endif
	va_end( arg );
}


/** ����� ��������� ��� �������. */

void dbgmsg(string sMsg)
{
/*
	FIXME: dbgmsg(sMsg.c_str()) �� ������������. ���������� ������ �� C-������,
	�� ������ string ��� �������� ��������� � ��������� �����. � ����� ���������
	���������� ����������������.
*/
	stringstream ss;
	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : " << sMsg << endl;

#ifdef DEBUG
	// ���� ���������� ����� ������ 10 000 000, �� �� ������� ���.
	// ������ �� ������������ ��������� � ������������ ������ ����
	static int iAmountLines = 0;
	iAmountLines++;
	if(iAmountLines > 1E+7) return;

	wzdebug.m_logfile << ss.str();
	wzdebug.m_logfile.flush();	
#else
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
 	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif
#endif

}


/** ����� ��������� ��� ������� */

void dbgmsg(char* msg)
{
	stringstream ss;
	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : " << msg << endl;

#ifdef DEBUG
	// ���� ���������� ����� ������ 10 000 000, �� �� ������� ���.
	// ������ �� ������������ ��������� � ������������ ������ ����
	static int iAmountLines = 0;
	iAmountLines++;
	if(iAmountLines > 1E+7) return;

	wzdebug.m_logfile << ss.str();
	wzdebug.m_logfile.flush();
#else
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
 	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif
#endif

}


/** ����� ��������� ��� ������� */

void dbgmsg(double value)
{
	stringstream ss;
	
	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : " << value << endl;

#ifdef DEBUG
	// ���� ���������� ����� ������ 10 000 000, �� �� ������� ���.
	// ������ �� ������������ ��������� � ������������ ������ ����
	static int iAmountLines = 0;
	iAmountLines++;
	if(iAmountLines > 1E+7) return;

	wzdebug.m_logfile << ss.str();
	wzdebug.m_logfile.flush();
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
 	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif
#endif
}


/** ������������ ������, ���������� ��������� */

void failed(char* msg)
{
	stringstream ss;

	ss << "<" << hthread() << "> ";
	ss << gettime();
	ss << " : Program failed. " << msg << endl;

#ifdef DEBUG
	wzdebug.m_logfile << ss.str();
	wzdebug.m_logfile.flush();
 #ifdef DEBUG2
 	if (wzdebug.m_logdeq.size() > WZ_LOGDEQ_SIZE) wzdebug.m_logdeq.pop_front();
 	wzdebug.m_logdeq.push_back(ss.str());
 #else
	cout << ss.str();
 #endif
#endif

	stop();
}
