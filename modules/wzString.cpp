/*
  Name: wzString.h
  Date: 17.05.2006
  Description: wzString class + some useful functions
*/


#include "wzString.h"


/** ������������ ������ � double */

wzString::operator double ()
{
	float dValue;
	stringstream ss;

	ss << *this;
	ss >> dValue;

	return dValue;
}


/** ������������ double-�������� � ������ */

wzString wzString::operator = (double dValue)
{
	stringstream ss;

	ss << dValue;
	ss >> *this;

	return *this;
}


/** �������������� ������ � ����� */

double StrToDbl(string sValue)
{
	float dValue;
	stringstream ss;

	ss << sValue;
	ss >> dValue;

	return dValue;
}

int StrToInteger(string sValue)
{
	int iValue;
	stringstream ss;

	ss << sValue;
	ss >> iValue;

	return iValue;
}

long long StrToLongLong(string sValue)
{
	long long llValue;
	stringstream ss;

	ss << sValue;
	ss >> llValue;

	return llValue;
}

/** �������������� ����� � ������
 * @param iDecimal = -1 - ��� ���������� ������� ��������� �����������,
 *                    0 - ��������� �� ������
 *                    n - �������� n ����� ����� �������
 */

string DblToStr(double dValue, int iDecimal)
{
	string sValue;
	stringstream ss;

	if (iDecimal >= 0) dValue = double ( int ( dValue * pow ( 10.0, double(iDecimal) ) ) / pow ( 10.0, double(iDecimal) ));
	
	ss << dValue;
	ss >> sValue;

	return sValue;
}

/** Converts an integer value to a string */

string IntToStr(int iValue)
{
	string sValue;
	stringstream ss;

	ss << iValue;
	ss >> sValue;

	return sValue;
}

string LongLongToStr(long long llValue)
{
	string sValue;
	stringstream ss;

	ss << llValue;
	ss >> sValue;

	return sValue;
}
/** ������� ������ �� ������ �����
 *   @param bSkipVoid = true - �� ��������� ������ ������ � ������ 
 *   @param sMain - ������ ������� ��������� �������
 *   @param sBr - ������ �����������
 */

vector<string> StrExplode(string sMain, string sBr, bool bSkipVoid)
{
	dbgmsg("vector<string> StrExplode(string sMain, string sBr, bool bSkipVoid)");

	vector<string> vsRet;

	if(sMain.length() < sBr.size()) return vsRet;

	int iStart = 0, iEnd = 0;

	if(sMain.substr(sMain.size() - sBr.size(), sBr.size()) != sBr) sMain += sBr;
	
	iEnd = sMain.find (sBr, iStart);
	
	while(iEnd >= 0 && iStart < sMain.size())
	{
		if (sMain.substr(iStart, iEnd - iStart).size() > 0)
			vsRet.push_back(sMain.substr(iStart, iEnd - iStart));
	
		iStart = iEnd + sBr.size();
		iEnd = sMain.find (sBr, iStart);
	}

	return vsRet;
}


/** ������� ������ ����� � ������ 
 *   @param vsMain - ������ �����
 *   @param sBr - �����������
 *   @param bSkipVoid - �� ��������� ������ ������-�������� �� �������
 */

string StrJoin(vector<string> vsMain, string sBr, bool bSkipVoid)
{
	vector<string>::iterator ivsIter;
	string sRet;
	if (vsMain.size() == 0) return sRet;
	for(ivsIter = vsMain.begin(); ivsIter != vsMain.end(); ++ivsIter)
		if ( (bSkipVoid && ivsIter->size() != 0) || (!bSkipVoid) )
			sRet += *ivsIter + sBr;
	
	return sRet;
}


/** �������� ������� �� ����� ������ */

string StrTrim(string sMain)
{
	dbgmsg("string StrTrim(string sMain)");

	// ��������� ������� ������� �� ������ ������? ��� �� ������ ������ �������
	if (sMain.length() == 0) return sMain;

	// ������� ������� � ������ ������
	while (sMain.length() > 0 ? sMain.at(0) == ' ' : false)
		sMain = sMain.substr(1,sMain.length()-1);
		
	// ������� ������� � ����� ������
	while (sMain.length() >0 ? sMain.at(sMain.length()-1) == ' ' : false)
		sMain = sMain.substr(0,sMain.length()-1);

	return sMain;
}


/** ��������� ��������� ������� � ������ �������. 
 * ����� ������� ������������� �������� ���������, ��������� ����� ������� ���������. */

string StrLCase(string sMain)
{
	dbgmsg("string StrLCase(string sMain)");

	for(int i = 0; i < sMain.size(); i++)
		sMain.at(i) = tolower(sMain.at(i));

	return sMain;
}

	
/** �������� ������������ �� ������� � ������� */

bool IsInSet(string sItem, vector<string> vsVec)
{
	// ���������� ��� ��������
	for (vector<string>::iterator ivsIter = vsVec.begin(); ivsIter != vsVec.end(); ++ivsIter)
		if (*ivsIter == sItem) return true;

	// ������� �� ������	
	return false;
}


/** ������ ��������� �� ��������.
 * ������: StrReplace("1234", "3", "x") = "12x4" */

string StrReplace(string sMain, string sFind, string sReplace)
{
	int iPos = sMain.find(sFind);
	while(iPos >= 0)
	{
		sMain.replace(iPos, sFind.size(), sReplace);
		iPos = sMain.find(sFind);
	}

	return sMain;
}
