/*
  Name: wzMySql.h
  Date: 05.04.2008
  Description:
*/


#ifndef _WZ_MYSQL_H_
#define _WZ_MYSQL_H_

#include <math.h>
#include <sstream>
#include <map>
#include <string.h>
#include <string>
#include <vector>
#ifdef WIN32
    #include <windows.h>
    #include "MySQL/mysql.h"
	#include <pthread.h>
#else
    #include "mysql.h"
#endif


#include "debug.h"

class wzMySql
{
	public:
		// �����������
		wzMySql();
		
		// �����������
		wzMySql(string sHostName, string sUserName, string sPassword, string sDatabaseName);
		
		// ����������
		~wzMySql();
		
		// ������������ � ���� ������
		bool Open(string sHostName, string sUserName, string sPassword, string sDatabaseName);
		
		// ������� ������
		bool Query(string sQuery);
		
		// ������� ����������� � ���� ������
		bool Close();
		
		// ���-�� ����� � ���������� �������
		int GetNumRows();
		
		// �������� ������ ��� ����� ������-������
		map<string, string> GetRow(int iRowNumber = 0);
		
		// �������� �������� ������ ���� ��� ����� �����
		string sErr();
		
		// �������� ����� ��������� ������
		int iErr();

	private:
		MYSQL* pMySql;
		MYSQL_RES* pResult;
};

#endif
