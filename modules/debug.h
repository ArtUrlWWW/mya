/**
 17.11.2006
 Debug functions

 12.02.2007
 FIXME: logfile-cout - ����������� ��� �������� ����. ���� ��������� ��
 ��������� �� �� ��������, �� � ����, �� ��������� �������� ��� ��������� �����
 ����������. ������� ���� ���� �� ����.

 ��� ���� ����� �������� ��� ��������� �������������� gcc ������ ������
  � ���������� -DDEBUG.
*/

#ifndef _WZ_DEBUG_H_
#define _WZ_DEBUG_H_

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/timeb.h>
#include <deque>
#include <sstream>

// ����� ��� ��������� ���� � ������� RFC1123
#define WZ_SERV_RFC1123FMT "%a, %d %b %Y %H:%M:%S"
#define WZ_SERV_RFC1123FMT_FILENAME "%a-%d-%b-%Y-%H-%M-%S"
#define WZ_LOGDEQ_SIZE 10000

using namespace std;

#ifdef WIN32
#include <windows.h>
#define hthread() GetCurrentThreadId()
#else
// UNIX/POSIX
#include <pthread.h>
#include <stdarg.h>
#define hthread() pthread_self()
#endif

class wzdebug
{
public:
	wzdebug();
	~wzdebug();

  	// ���� ��� ������ ����� � �������� ������ ���������
	ofstream m_logfile;
	// ������� ���-���������
	deque<string> m_logdeq;
};


/** ���������� ����� �����. */

void DbgSaveDump(void);


/** ��������� ������� � ������� RFC. sizeof(timebuff) >= 50 */

void gettime(char* timebuf);


/** ��������� ���������. */

void stop();


/** ����� ��������� � ����� printf. */

void dprintf(string fmt, ...);


/** ����� ��������� ��� �������. */

void dbgmsg(string sMsg);


/** ����� ��������� ��� �������. */

void dbgmsg(char* msg);


/** ����� ������������ ��������� ��� �������. */

void dbgBigMsg(char* msg);


/** ����� ��������� ��� �������. */

void dbgmsg(double value);


/** ������������ ������, ���������� ���������. */

void failed(char* msg);

#endif
