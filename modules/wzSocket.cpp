/*
  Name: wzSocket.cpp
  Date: 17.05.2007
  Description: Crossplatform socket class (POSIX, Windows)
*/


#include "wzSocket.h"

/** ����������� */
wzSocket::wzSocket()
{
	m_bIsListen = false;
	m_IP = 0;
	hSocket = 0;
	
	#ifdef WIN32
	static bool bWSAInitOK = false;
	if (!bWSAInitOK)
	{
		WSADATA ws;
		if (WSAStartup (0x0202, &ws) == 0) bWSAInitOK = true;
	}
	#endif
}

/** ���������� */
wzSocket::~wzSocket()
{
	//Close();
}

/** �������� ������ */
bool wzSocket::Send (string sData)
{
	if (hSocket <= 0)
	{
	    //!!cout << "bool wzSocket::Send (string s) : hSocket <= 0\n";
	    return false;
	}

	int iSendDataSize = 0;
	iSendDataSize = send(hSocket, sData.data(), sData.size(), MSG_NOSIGNAL);

	////!!cout << "bool wzSocket::Send (string sData)::iSendDataSize = " << iSendDataSize << endl;
	return ( iSendDataSize == sData.size() );
}
		
/** ������� ������ */
bool wzSocket::Recv (string& sData, int iMaxLen)
{
	if (hSocket <= 0)
	{
	    //!!cout << "bool wzSocket::Recv (string& s, int iLength) : hSocket <= 0\n";
	    return false;
	}

//	//!!cout << "hSocket=" << hSocket << endl;

	int iRecvLen = 0;
	char* chBuff = new char [iMaxLen];
	if (chBuff == NULL) return false;
	
	if ( (iRecvLen = recv(hSocket, chBuff, iMaxLen, MSG_NOSIGNAL)) <= 0 )
	{
		sData.clear();
		//!!cout << "iRecvLen=" << iRecvLen << endl;
		delete chBuff;
		return false;
	}

	sData.assign(chBuff, iRecvLen);
	delete chBuff;
	return true;
}

/** �������� ����� ��  ������������� 
@return true - �������� ����������� ������ */
bool wzSocket::Listen(int iPort)
{
	if (iPort >= 65535 || iPort <= 0) return false;
	
	m_bIsListen = false;
	
	hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (hSocket <= 0) return false;
	int iOn = 1;
	if (setsockopt(hSocket, SOL_SOCKET, SO_REUSEADDR, (const char* ) &iOn, sizeof(iOn)) == -1) return false;

	// ������ ����� �����������
	#ifdef WIN32
	u_long uiOn = 0;
	ioctlsocket(hSocket, FIONBIO, &uiOn);
	#endif

	sockaddr_in adr;
	adr.sin_family = AF_INET;
	adr.sin_port = htons(iPort);
	adr.sin_addr.s_addr = INADDR_ANY;

	if (bind(hSocket, (struct sockaddr*) &adr, sizeof(adr)))
	{
	    //!!cout << "bind failed\n";
	    return false;
	}
	
	if (listen(hSocket, SOMAXCONN))
	{
	    //!!cout << "listen failed\n";
	    return false;
	}
	
	m_bIsListen = true;
	return true;
}

/** ������� ����������� �� ����� wsAccept. ���������������, ��� ������� ����� 
��������� � ������ ���������. */
bool wzSocket::Accept(wzSocket& wsAccept)
{
	sockaddr_in saAddr;
	int iAddrSize = sizeof(saAddr);

	memset(&saAddr, 0, sizeof(saAddr));

	SOCKET sckAcc = accept(hSocket, (sockaddr *) &saAddr, (socklen_t*) &iAddrSize);

	//!!cout << "wzSocket wzSocket::Accept()::sckAcc=" << sckAcc << endl;

	if (sckAcc <= 0 || iAddrSize <= 0) return false;

	wsAccept.m_bIsListen = false;
	wsAccept.hSocket = sckAcc;

	wsAccept.m_IP = saAddr.sin_addr.s_addr;

	//!!cout << "wsAccept.m_IP=" << wsAccept.m_IP << endl;

	return true;
}

/** ������������ ������� */
void wzSocket::operator = (wzSocket sckNew)
{
	hSocket = sckNew.hSocket;
	m_IP = sckNew.m_IP;
	m_bIsListen = sckNew.m_bIsListen;
}

// Returns IP of the socket
unsigned int wzSocket::ip()
{
	return m_IP;
}

/** ����������� � ����� */
bool wzSocket::Connect(string sHostname, int iPort)
{
	if (iPort >= 65535 || iPort <= 0 || hSocket <= 0) return false;

	sockaddr_in addr;
	hostent *hn;

	if (NULL == (hn = gethostbyname (sHostname.c_str())) )
		return false;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(iPort);
	addr.sin_addr.s_addr = *(int* ) hn->h_addr_list[0];

	if (connect(hSocket, (sockaddr*) &addr, sizeof(addr)) < 0) return false;
	else return true;
}

/** ������� ����� */
bool wzSocket::Close()
{
	if (hSocket <= 0) return false;
	if (!m_bIsListen) shutdown(hSocket, 2);

	#ifdef WIN32
		closesocket(hSocket);
	#else
		close (hSocket);
	#endif

	hSocket = 0;
	return true;
}

/** ������� TCP ����� ��� ������ */
bool wzSocket::ShutdownRD()
{
	shutdown(hSocket, 0);
}

/** ������� TCP ����� ��� ������ */
bool wzSocket::ShutdownWR()
{
	shutdown(hSocket, 1);
}

/** ������� TCP ����� ��� ������ � ������ */
bool wzSocket::ShutdownRDWR()
{
	shutdown(hSocket, 2);
}

/** ���������� ������� �� �����. 0 - ����������� ������� */
void wzSocket::SetRecvTimeout(unsigned int sec, unsigned int usec)
{
	struct timeval tv;
	tv.tv_sec = 30;
	tv.tv_usec = 0;
  	setsockopt(hSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));	
}

/* �������� ������ �� ����������
int opts = fcntl (csocket, F_GETFL);
if (opts < 0)
{
	dbgmsg("socket opts is invalid");
	continue;
}
*/
