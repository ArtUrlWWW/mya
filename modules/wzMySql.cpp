/*
  Name: wzMySql.h
  Date: 05.04.2008
  Description:
*/

#include "wzMySql.h"

using namespace std;

wzMySql::wzMySql()
{
	pMySql = NULL;
	pResult = NULL;
}

wzMySql::wzMySql(string sHostName, string sUserName, string sPassword, string sDatabaseName)
{
	pMySql = NULL;
	pResult = NULL;
}

wzMySql::~wzMySql()
{
	Close();
}

bool wzMySql::Open(string sHostName, string sUserName, string sPassword, string sDatabaseName)
{
	Close();
	
	pMySql = mysql_init(NULL);
	if (mysql_real_connect(pMySql, sHostName.c_str(), sUserName.c_str(), sPassword.c_str(), sDatabaseName.c_str(), 0, NULL, 0) == NULL) return false;
	if (pMySql == NULL) return false;
	else return true;
}

bool wzMySql::Query(string sQuery)
{
	// ��� ����������� � ���� ������
	if (pMySql == NULL) return false;
	
	if (pResult != NULL)
	{
		mysql_free_result(pResult);
		pResult = NULL;
	}
	
	if (mysql_real_query(pMySql, sQuery.data(), sQuery.size()) != 0) return false;
	
	pResult = mysql_store_result(pMySql);
	
	return true;
}

bool wzMySql::Close()
{
	if (pMySql == NULL) return false;
	
	if (pResult != NULL)
	{
		mysql_free_result(pResult);
		pResult = NULL;
	}
	
	mysql_close(pMySql);
	pMySql = NULL;
	
	return true;
}

int wzMySql::GetNumRows()
{
	if (pResult != NULL) return mysql_num_rows(pResult);
	else return -1;
}

map<string, string> wzMySql::GetRow(int iRowNumber)
{
	map <string, string> smRet;

	if (iRowNumber < 0 || iRowNumber > GetNumRows() - 1 || pResult == NULL) return smRet;	
	mysql_data_seek(pResult, iRowNumber);
	
	MYSQL_ROW myRow;	
	unsigned int iNumFields = mysql_num_fields(pResult);
	MYSQL_FIELD* myFields = mysql_fetch_fields(pResult);
	myRow = mysql_fetch_row(pResult);
	
	if (myRow == NULL) return smRet;
	
	for(unsigned int i = 0; i < iNumFields; i++)
		smRet[myFields[i].name] = myRow[i] != NULL ? myRow[i] : "";

	return smRet;
}

int wzMySql::iErr()
{
	if (pMySql == NULL) return 0;
	
	return mysql_errno(pMySql);
}

string wzMySql::sErr()
{
	if (pMySql == NULL) return "";
	
	string sRet;
	char* pszDescription = (char*) mysql_error(pMySql);
	sRet.assign(pszDescription, strlen(pszDescription));
	
	return "MySqlError: "+sRet;
}
