/*
  Name: wzThread.cpp
  Date: 18.05.2007
  Description: Thread abstract class. Provides cross-platform unified interface 
   for thread manipulation.
*/


#include "wzThread.h"

/** �����������. */
wzThread::wzThread()
{
	#ifdef WIN32
		m_hThreadId = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) &ThreadStart, (PVOID) this, 0, NULL);
	#else
		if (pthread_create(&m_hThreadId, NULL, &ThreadStart, this) == 0)
		    pthread_detach(m_hThreadId);
	#endif
}

/** ����������. ������������� ��������� ��������� �����. */
wzThread::~wzThread()
{
	#ifdef WIN32
		TerminateThread(m_hThreadId, 0);
	#else
		pthread_cancel(m_hThreadId);
	#endif
}

/** ����������� ������� ������� ���������� ���������� ����� �������� ������. ������ �-�� ��������� ��� ThreadFunction. */
void* wzThread::ThreadStart(void* objThis)
{
	wzThread* This = (wzThread*) objThis;
	This->ThreadFunction();
}
