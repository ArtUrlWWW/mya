# Project: wzServer

CPP  = g++
CC   = gcc
OBJ  = ../bin/mya.o ../bin/debug.o ../bin/wzString.o ../bin/wzMutex.o ../bin/wzThread.o ../bin/wzMySql.o ../bin/wzSocket.o ../bin/wzHttp.o
LINKOBJ  = ../bin/mya.o ../bin/debug.o ../bin/wzString.o ../bin/wzMutex.o ../bin/wzThread.o ../bin/wzMySql.o ../bin/wzSocket.o ../bin/wzHttp.o
LIBS =  -lpthread /usr/lib/mysql/libmysqlclient.so.16
CXXINCS = -I"."  -I"../modules" -I"../mysql"
BIN  = ../bin/mya
CXXFLAGS = $(CXXINCS) -DDEBUGNO
RM = rm -f

.PHONY: clean

all: ../bin/mya

clean:
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CPP) $(LINKOBJ) -o "../bin/mya" $(LIBS)

../bin/mya.o: mya.cpp
	$(CPP) -c mya.cpp -o ../bin/mya.o $(CXXFLAGS)

../bin/debug.o: debug.cpp
	$(CPP) -c debug.cpp -o ../bin/debug.o $(CXXFLAGS)

../bin/wzString.o: wzString.cpp
	$(CPP) -c wzString.cpp -o ../bin/wzString.o $(CXXFLAGS)

../bin/wzMutex.o: wzMutex.cpp
	$(CPP) -c wzMutex.cpp -o ../bin/wzMutex.o $(CXXFLAGS)

../bin/wzThread.o: wzThread.cpp
	$(CPP) -c wzThread.cpp -o ../bin/wzThread.o $(CXXFLAGS)

../bin/wzMySql.o: wzMySql.cpp
	$(CPP) -c wzMySql.cpp -o ../bin/wzMySql.o $(CXXFLAGS)

../bin/wzSocket.o: wzSocket.cpp
	$(CPP) -c wzSocket.cpp -o ../bin/wzSocket.o $(CXXFLAGS)

../bin/wzHttp.o: wzHttp.cpp
	$(CPP) -c wzHttp.cpp -o ../bin/wzHttp.o $(CXXFLAGS)
