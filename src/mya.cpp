/*
  Name: mya.cpp
  Date: 27.12.2008, 19:39 13.01.2009, 22:32 10.07.2010
  Description: HTTP-server.
  
  FIXED:
  	1. +����������� ��������
	2. +����������� �������
	3. +����������� �������
	4. -��������� ����� ����� ������
	5. +����� ���� � ����� �� �� �� ������� Admin_dvig.files;
	6. +���������� ��� Windows
	7. +�������� ����� ���������� ������������ �� ������� ����� HTTP (���� ����
        �� ��������, ������� ����� ����� ������, ���� ������ �� ���������)
	
*/

#include "mya.h"

#ifdef WIN32
	#include <winbase.h>
	#define sleep Sleep
#endif


using namespace std;

/** ����� IP. �������� ���������� ����������� � ��������� IP */
map <unsigned int, int> mConnectionsFromIP;

/** ���������� ������������ �������� */
int iRequestCount = 0;

int get_unix_time()
{
	timeval tv;
	gettimeofday(&tv, NULL);

	return tv.tv_sec;
}

long long getFileSize(string sFileName)
{
	cout << "int getFileSize(string sFileName)\n";
	
	FILE* f = fopen(sFileName.data(), "rb");
	
	if (!f)
	{
		cout << "Error: can't open file " << sFileName << endl;
		return -1;
	}
	
	fseek(f, 0, SEEK_END);
	
	long long i = ftell(f);
	fclose(f);
	
	return i;
}

/** ����� ����� � �� �� fileid */

map <string,string> GetFileInfoByID(string iFileID, string iUserID)
{
	wzMySql mysql;
	mysql.Open(DBHOST, DBUSER2, DBPASS2, DBNAME2);
//	mysql.Query("SET NAMES CP1251");
	mysql.Query("SET NAMES utf8");
    if (mysql.iErr() != 0) cout << mysql.sErr() << endl;

	mysql.Query("SELECT * FROM files WHERE id like '" + iFileID + "'");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	
	map <string, string> sm;
	sm["fullpath"] = "";
	
	// ��� ������ �����. ��������� ����������.
	if (mysql.GetNumRows() == 1)
	{	
		if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	
		sm  = mysql.GetRow(0);
	
		cout << "Database: get record in a files table.\n";
		cout << "id=" << sm["id"] << endl;
		cout << "name=" << sm["name"] << endl;
		cout << "path=" << sm["path"] << endl;
		cout << "fullpath=" << sm["fullpath"] << endl;
		cout << "isutfff=" << sm["ifutfff"] << endl;
		cout << "filesize=" << sm["filesize"] << endl;
		cout << "downloads=" << sm["downloads"] << endl;
	} 
	else cout << "Database error: file id not found in a files table.\n";
	
	mysql.Close();
	
	sm["name"] = "\"" + sm["name"] + "\""; // � �������� ����� ����� ����������� ������
	
	return sm;
}


/** ��������� ������� � ��������� ������ */

void* ServerThreadFunction(void* p)
{
	cout << "RequestCount = " << ++iRequestCount << endl;

	wzSocket* pc = (wzSocket*) p;

	string sHttpRequest, sBuff, sUrl;
	while (GetHttpHeaderLen(sHttpRequest) <= 0)
	{
		if(!pc->Recv(sBuff, 1000)) break;
		sHttpRequest += sBuff;
	}
	
	cout << sHttpRequest;
	
	string iUserID = ""; 
	string iFileID = "";
	
	vector <string>	vsLines = StrExplode(sHttpRequest, "\xD\xA"), vsParams;
	sHttpRequest = "";
	if (vsLines.size() > 0)
	{
		// ��������� URL
		vsParams = StrExplode(vsLines.at(0), " ");
		if (vsParams.size() == 3)
		{
			if (StrLCase(vsParams[0]) == "get")
			{
				sUrl = StrReplace(vsParams.at(1), "/?", "");
				cout << "sUrl=" << sUrl << endl;
				vsParams = StrExplode(sUrl, "&");
			
				for (int i = 0; i < vsParams.size(); i++)
				{
					vector <string> vsGETPair = StrExplode(vsParams.at(i), "=");
					if (vsGETPair.size() == 2)
					{
						if (StrLCase(vsGETPair.at(0)) == "userid") iUserID = vsGETPair.at(1);
						if (StrLCase(vsGETPair.at(0)) == "fileid") iFileID = vsGETPair.at(1);
					}
				}
			}
			else
			{
				pc->Send("Bad URL.");
                pc->Close();
	
				delete pc;
				pthread_exit(NULL);	
			}
		}
		else
		{
			pc->Close();

			delete pc;
			pthread_exit(NULL);		
		}
	}
	else
	{
		pc->Close();
	
		delete pc;
		pthread_exit(NULL);	
	}

	cout << "iUserID, iFileID = " << iUserID << ", " << iFileID << endl;	

	wzMySql mysql;
	mysql.Open(DBHOST, DBUSER, DBPASS, DBNAME);
	mysql.Query("SET NAMES utf8");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;

	mysql.Query("SELECT * FROM mya WHERE userid=" + iUserID + " && fileid like '" + iFileID+"'");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;

	// ��� ������ � �����, �����, ���������� ����������. ��������� ����������.
	if (mysql.GetNumRows() != 1)
	{
		cout << "Error: database record not found.\n";
		mysql.Close();
		pc->Send("No information found about the requested file.");
		pc->Close();
	
		delete pc;
		pthread_exit(NULL);
	}
	
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	
	map <string, string> sm = mysql.GetRow(0);
	mysql.Close();
	
	cout << "lifetime=" << sm["lifetime"] << endl;
	cout << "speed=" << sm["speed"] << endl;
	cout << "dokachka=" << sm["dokachka"] << endl;
	cout << "potokov=" << sm["potokov"] << endl;

	// ����� ����� ������ ���������
	if (get_unix_time() > StrToInteger(sm["starttime"]) + StrToInteger(sm["lifetime"]))
	{
		cout << "info: time > starttime + lifetime. close.\n";
		pc->Send("This link is expired.");
	
		pc->Close();
		delete pc;
		pthread_exit(NULL);		
	}
	

	// ������� ������ ��� ���������, ��������� ����������
	if (mConnectionsFromIP[pc->ip()] >= StrToInteger(sm["potokov"]))
	{
		cout << "info: you have many connections. close.\n";
	
		pc->Close();
		delete pc;
		pthread_exit(NULL);		
	}
	// �������������� ���������� ��������� �� ������� IP
	mConnectionsFromIP[pc->ip()]++;

	map <string, string> smFileInfo = GetFileInfoByID(iFileID, iUserID);
	long long iFileSize = getFileSize(smFileInfo["fullpath"].c_str());

	// �� ����� ���� � ������ ID, ���� ���� �� ����������� -  ��������� ����������.
	if (smFileInfo["name"] == "" || iFileSize < 0)
	{
		cout << "Error: file not found or can't open.\n";
		
		mConnectionsFromIP[pc->ip()]--;
		if (mConnectionsFromIP[pc->ip()] == 0) mConnectionsFromIP.erase(pc->ip());
		
		pc->Send("File not found (in database or in repository).");
		pc->Close();
		delete pc;

		pthread_exit(NULL);	
	}

	// ��������� ���������� � ����� �� ����. �����
	int iStartTime = get_unix_time();
	//wzMySql mysql2;
	mysql.Open(DBHOST, DBUSER, DBPASS, DBNAME);
	mysql.Query("SET NAMES utf8");
	mysql.Query("CREATE TABLE dlstat(Id int, StartTime int)");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	cout << "create dlstat table\n";

	mysql.Query("INSERT INTO dlstat(Id, StartTime) values('" + iFileID + "', " + IntToStr(iStartTime) + ")");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	cout << "insert into dlstat table\n";
	mysql.Close();

	long long iStartPos = 0;
		
	for (int i = 1; i < vsLines.size(); i++)
	{
		vsParams = StrExplode(vsLines.at(i), ": ");
		if (vsParams.size() == 2)
		{
			cout << vsParams.at(0) << " " << vsParams.at(1) << endl;
			if (vsParams.at(0) == "Range")
			{
				vsParams = StrExplode(vsParams.at(1), "=");
				cout << vsParams.at(1) << endl;
				iStartPos = StrToInteger(vsParams.at(1).substr(0, vsParams.at(1).length()-1));
				cout << "Start position: " << iStartPos << endl;
			}
		}
	}

	string sHttpHeader;

	if (iStartPos == 0) sHttpHeader = "HTTP/1.1 200 OK\r\nContent-Disposition: attachment; filename=" + smFileInfo["name"] + ";\r\nContent-Type: application; charset=windows-1251\r\nConnection: close\r\nServer: Mya-v1.0\r\nAccept-Ranges: bytes\r\nContent-Length: " + LongLongToStr(iFileSize - iStartPos) + "\r\nContent-Range: bytes " + LongLongToStr(iStartPos) + "-" + LongLongToStr(iFileSize - 1) + "/" + LongLongToStr(iFileSize) + "\r\n\r\n";
	else
	{
		if (sm["dokachka"] == "1") sHttpHeader = "HTTP/1.1 206 Partial Content\r\nContent-Disposition: attachment; filename=" + smFileInfo["name"] + ";\r\nContent-Type: application; charset=windows-1251\r\nConnection: close\r\nServer: Mya-v1.0\r\nAccept-Ranges: bytes\r\nContent-Length: " + LongLongToStr(iFileSize - iStartPos) + "\r\nContent-Range: bytes " + LongLongToStr(iStartPos) + "-" + LongLongToStr(iFileSize - 1) + "/" + LongLongToStr(iFileSize) + "\r\n\r\n";
		else
		{
			// ��� �������, ������ �������� ����
			iStartPos= 0;
			sHttpHeader = "HTTP/1.1 200 OK\r\nContent-Type: application; charset=windows-1251\r\nContent-Disposition: attachment; filename=" + smFileInfo["name"] + ";\r\nConnection: close\r\nServer: Mya-v1.0\r\nAccept-Ranges: bytes\r\nContent-Length: " + LongLongToStr(iFileSize - iStartPos) + "\r\nContent-Range: bytes " + LongLongToStr(iStartPos) + "-" + LongLongToStr(iFileSize - 1) + "/" + LongLongToStr(iFileSize) + "\r\n\r\n";
		}
	}
	
	pc->Send(sHttpHeader);
	
	cout << sHttpHeader;
	
	FILE* f = fopen(smFileInfo["fullpath"].data(), "rb");
	fseek (f, iStartPos, SEEK_SET);
	
	if (!f)
	{
		cout << "Error: file not found.\n";
		pc->Send("Error: file not found.");
	}
	else
	{
		string sData;
		sData.resize(1000);
	
		int iCnt = 0, n = 0;
		while ((n = fread ((char*) sData.data(), 1, 1000, f)) > 0)
		{
			if (n > 0)
			{
				if (n != 1000) sData.resize(n);
				if (!pc->Send (sData))
				{
					// ���������� �������.
					cout << "client close connection.\n";
					break;
				}
				
				cout << "send 1kbytes data. iCnt=" << iCnt++ << endl;
				
				if (StrToInteger(sm["speed"]) == 0) sm["speed"] = 1000;
				
				usleep(useconds_t(1000.0 / float(StrToInteger(sm["speed"])) * 1000000));
			}
			else cout << "send end\n";
		}
		
		fclose(f);
	}
	
	cout << "file send OK\n";

	cout << "delete stat info\n";
	//wzMySql mysql3;
	mysql.Open(DBHOST, DBUSER, DBPASS, DBNAME);
	mysql.Query("SET NAMES utf8");
	mysql.Query("DELETE FROM dlstat WHERE StartTime=" + IntToStr(iStartTime) + " && Id like '" + iFileID + "'");
	if (mysql.iErr() != 0) cout << mysql.sErr() << endl;
	mysql.Close();

	mConnectionsFromIP[pc->ip()]--;
	if (mConnectionsFromIP[pc->ip()] == 0) mConnectionsFromIP.erase(pc->ip());

	pc->Close();	
	delete pc;
	pthread_exit(NULL);
}


/** ����� ����� */

int main(int argc, char* argv[])
{
	cout << "Program: Mya.\nDescription: HTTP-server for library ihtika.net.\nLast modification date: 10.07.2010.\nArguments: -d - debug mode, -h - show help.\n\n";
    if (argc > 1)
	{        
		if (string(argv[1]) == "-d")
		{
			cout << "debug mode...\n";
		}
		else if (string(argv[1]) == "-h")
		{
			cout << "\nLast modification date: 10.07.2010.\nProgram: Mya.\nDescription: HTTP-server for library ihtika.net.\nArguments: -d - debug mode, -h - show help.\n\n";
			return 0;
		}
		else
		{
			cout << "run as daemon OK\n";
//vv			daemon(1,0);
		}
	}
	else
	{
		cout << "run as daemon OK\n";
//vv		daemon(1,0);	
	}

	wzSocket s,*c;
	
	s.Listen(LISTEN_PORT);
	cout << "listen port: " << LISTEN_PORT << endl;
	
	pthread_t ptHandle;
	
	// ��������� ����������� � ������� ������ ��� ���������
	while(true)
	{
		c = new wzSocket();
		if (c)
		{
			s.Accept(*c);
			if (pthread_create(&ptHandle, NULL, &ServerThreadFunction, c) == 0) pthread_detach(ptHandle);
		}
		else cout << "Error: can't create socket.\n";
		sleep(1);
	}
	
	s.Close();

	return 0;
}

