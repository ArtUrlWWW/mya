﻿-- ----------------------------
-- Table structure for `mya`
-- ----------------------------
DROP TABLE IF EXISTS `mya`;
CREATE TABLE `mya` (
  `fileid` varchar(777) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `starttime` int(11) NOT NULL DEFAULT '0',
  `lifetime` int(11) NOT NULL DEFAULT '0',
  `speed` int(11) NOT NULL DEFAULT '0',
  `dokachka` int(11) NOT NULL DEFAULT '0',
  `potokov` int(11) NOT NULL DEFAULT '0',
  KEY `i_fileid` (`fileid`(333)),
  KEY `i_userid` (`userid`),
  KEY `i_starttime` (`starttime`),
  KEY `i_lifetime` (`lifetime`),
  KEY `i_speed` (`speed`),
  KEY `i_dokachka` (`dokachka`),
  KEY `i_potokov` (`potokov`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mya
-- ----------------------------
INSERT INTO `mya` VALUES ('96341109373636a97832514dd6660ed6', '1', '1277136488', '77777', '2500', '1', '2');

/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50148
Source Host           : localhost:3306
Source Database       : admin_dvig

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2010-06-21 22:39:21
*/

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(777) NOT NULL,
  `name` varchar(777) NOT NULL,
  `fname` varchar(777) NOT NULL,
  `pass` varchar(777) NOT NULL,
  `sw` varchar(777) NOT NULL,
  `emailll` varchar(230) NOT NULL,
  `balance` varchar(777) NOT NULL DEFAULT '0',
  `statusvip` int(11) NOT NULL DEFAULT '1',
  `endofvip` int(11) NOT NULL DEFAULT '0',
  `dfiles` int(11) DEFAULT '0',
  `refpereh` int(255) NOT NULL DEFAULT '0',
  `refdowns` int(255) NOT NULL DEFAULT '0',
  `whoreferer` int(255) NOT NULL DEFAULT '0',
  `bfr` varchar(777) NOT NULL DEFAULT '0',
  `userrole` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_statusvip` (`statusvip`),
  KEY `i_endofvip` (`endofvip`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'q', 'q', 'q', 'q', 'q', 'q', '0', '1', '0', '0', '0', '0', '0', '0', '0');