SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` varchar(777) NOT NULL,
  `name` varchar(777) NOT NULL,
  `path` varchar(2300) default NULL,
  `fullpath` varchar(2300) default NULL,
  `filesize` varchar(777) NOT NULL default '0',
  `filesizebyte` int(255) NOT NULL default '0',
  `downloads` int(255) NOT NULL default '0',
  `ocenka` int(255) NOT NULL default '0',
  `golosov` int(255) NOT NULL default '0',
  `parsed` int(255) NOT NULL default '0',
  `folderid` int(255) NOT NULL default '0',
  KEY `i_fullpath` (`fullpath`(1000)),
  KEY `i_name` (`name`),
  KEY `i_id` (`id`),
  KEY `i_path` (`path`(1000)),
  KEY `i_folderid` (`folderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- ���� ������: `admin_mya`
--

-- --------------------------------------------------------

--
-- ��������� ������� `dlstat`
--

DROP TABLE IF EXISTS `dlstat`;
CREATE TABLE `dlstat` (
  `Id` int(11) default NULL,
  `StartTime` int(11) default NULL,
  KEY `i_Id` (`Id`),
  KEY `i_StartTime` (`StartTime`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `mya`
--

DROP TABLE IF EXISTS `mya`;
CREATE TABLE `mya` (
  `fileid` int(11) NOT NULL default '0',
  `userid` int(11) NOT NULL default '0',
  `starttime` int(11) NOT NULL default '0',
  `lifetime` int(11) NOT NULL default '0',
  `speed` int(11) NOT NULL default '0',
  `dokachka` int(11) NOT NULL default '0',
  `potokov` int(11) NOT NULL default '0',
  KEY `i_fileid` (`fileid`),
  KEY `i_userid` (`userid`),
  KEY `i_starttime` (`starttime`),
  KEY `i_lifetime` (`lifetime`),
  KEY `i_speed` (`speed`),
  KEY `i_dokachka` (`dokachka`),
  KEY `i_potokov` (`potokov`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(777) NOT NULL,
  `name` varchar(777) NOT NULL,
  `fname` varchar(777) NOT NULL,
  `pass` varchar(777) NOT NULL,
  `sw` varchar(777) NOT NULL,
  `emailll` varchar(230) NOT NULL,
  `balance` varchar(777) NOT NULL default '0',
  `statusvip` int(11) NOT NULL default '1',
  `endofvip` int(11) NOT NULL default '0',
  `dfiles` int(11) default '0',
  `refpereh` int(255) NOT NULL default '0',
  `refdowns` int(255) NOT NULL default '0',
  `whoreferer` int(255) NOT NULL default '0',
  `bfr` varchar(777) NOT NULL default '0',
  `userrole` int(5) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `i_statusvip` (`statusvip`),
  KEY `i_endofvip` (`endofvip`)
) ENGINE=MyISAM
